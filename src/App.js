import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch  } from 'react-router-dom';
// import { Redirect } from 'react-router-dom';
import * as uuid from 'uuid';
import Navbar from './components/layout/Navbar';
// import Footer from './components/layout/Footer';
import Education from './components/pages/Education';
import About from './components/pages/About';
import Projects from './components/pages/Projects';
import Skills from './components/pages/Skills';


class App extends React.Component {

  state = {

    projects: [

      {
        id: uuid.v4(),
        title: 'MMO Assistant',
        description: 'App to assist players on how to approach ingame content based on current progress',
        tech: 'Made versions in Laravel and React with Redux and Python (Flask) and Vue.js with Vuex',
        // image: "https://i.imgur.com/mZpUq59.png",
        image: "https://i.imgur.com/w1TSbYE.png",
        git: "https://gitlab.com/Oduu/osrs-slayer"
        // git: "https://gitlab.com/Oduu/osrs-slayer-helper"
      },

      {
        id: uuid.v4(),
        title: 'Film Reviews',
        description: 'Web scraping project combined with message board project to create a film review site',
        tech: 'Python, Webscraping (BeautifulSoup), JavaScript, MySQL, HTML, CSS, Axios, AJAX',
        image: 'https://i.imgur.com/QOzXDBb.png',
        git: "https://gitlab.com/Oduu/imdb-scraper"
      },

      {
        id: uuid.v4(),
        title: 'Todo List',
        description: 'Simple todo list app',
        tech: 'Vue.js, MongoDB, JavaScript, CSS, HTML, Python',
        image: 'https://i.imgur.com/ZkVdMrA.png',
        git: "https://gitlab.com/Oduu/todo-tracker"
      },

      {
        id: uuid.v4(),
        title: 'Message Board',
        description: 'Reddit style post/comment site',
        tech: 'Python, HTML, CSS, MySQL, Bcrypt',
        image: 'https://i.imgur.com/SXbmLmY.png',
        git: "https://gitlab.com/Oduu/message-board-v2"
      }

    ]

  }

  render() {
    return (
      <Router basename={`${process.env.PUBLIC_URL}`}>
        <Navbar />
        <div className="App" >

         {/*style={{ background: `url(${process.env.PUBLIC_URL}/background.jpg`  }}*/}
        
          <Switch>
            <Route exact path="/" component={About} />

            <Route exact path="/about" component={About} />

            <Route path="/education" component={Education} />

            <Route path="/skills" component={Skills} />

            <Route path="/projects" render={props => (
              <React.Fragment>
                <Projects projects={this.state.projects}/>
              </React.Fragment>
              )} />

            <Route component={About} />
            
          </Switch>
          
        </div>

      </Router>
    );
  }
}

export default App;
