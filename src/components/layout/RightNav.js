import React from 'react';
import '../../css/RightNav.css';
import {Link} from 'react-router-dom';

export default class RightNav extends React.Component {
    static propTypes = {
        
    };

    // constructor(props) {
    //     super(props);
    // }

    render() {
        return (
            <div className={(this.props.open ? 'flex' : 'hide')}>
                <ul id="link-list">

                    <Link to="/home" onClick={this.props.toggleMenu}>
                        <li className="nav-link">Home</li>
                    </Link>

                    <Link to="/projects" onClick={this.props.toggleMenu}>
                        <li className="nav-link">Projects</li>
                    </Link>

                    <Link to="/skills" onClick={this.props.toggleMenu}>
                        <li className="nav-link">Skills</li>
                    </Link>

                    <Link to="/education" onClick={this.props.toggleMenu}>
                        <li className="nav-link">Education</li>
                    </Link>

                </ul>
            </div>
        );
    }
}
