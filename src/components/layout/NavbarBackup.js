import React from 'react';
import '../../css/Navbar.css';
import {withRouter} from 'react-router-dom';
import {Link} from 'react-router-dom';

class Navbar extends React.Component {

  state = {
    active: this.props.location.pathname
  }

  componentDidMount() {
    window.addEventListener('scroll', () => {
      const isTop = window.scrollY > 100;
      const nav = document.getElementById('navbar');
      if(isTop) {
        nav.classList.add('scrolled');
      } else {
        nav.classList.remove('scrolled');
      }
    });
  }

  componentWillUnmount() {
    window.removeEventListener('scroll');
  }; 

  render() {

    const { active } = this.state;

    return (
      <div id="navbar" className="navbar">

        <div className="navbar-buttons">
          
          <a className={active === "/" ? 'active navbar-button': 'navbar-button'} href={`${process.env.PUBLIC_URL}/`}>Home</a>
          <a className={active === "/projects" ? 'active navbar-button': 'navbar-button'} href={`${process.env.PUBLIC_URL}/projects`}>Projects</a>
          <a className={active === "/skills" ? 'active navbar-button': 'navbar-button'} href={`${process.env.PUBLIC_URL}/skills`}>Skills</a>
          <a className={active === "/education" ? 'active navbar-button': 'navbar-button'} href={`${process.env.PUBLIC_URL}/education`}>Education</a>
          
          
          
        </div>

      </div>


    );
  }
}

export default withRouter(Navbar);
