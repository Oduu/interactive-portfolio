import React from 'react';
import '../../css/Footer.css';

function Footer() {

		return (
			<div className="footer">

				<div className="footer-column">

					<div className="contact">
						<h5 style={{ paddingBottom: 0 }}>Contact</h5>
						<h5>07749798992</h5>
						<h5>reddinghf@gmail.com</h5>
					</div>

				</div>

				<div className="footer-column">

					<div className="react">
						<h5>Project made in React</h5>
						<img src="https://tleunen.github.io/react-mdl/react.svg" alt="react" />
					</div>

				</div>

				<div className="footer-column">

					<div className="social-media">
						<h5>Follow</h5>
						<a href="https://www.linkedin.com/in/henry-redding-86923115a/">	• LinkedIn</a>
						<a href="https://gitlab.com/users/Oduu/projects"> 	• GitLab</a>
					</div>

				</div>

			</div>
		)
	
}

export default Footer