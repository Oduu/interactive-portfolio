import React from 'react';
import PropTypes from 'prop-types';

export class TechSkillItem extends React.Component {
	render() {

		const { name } = this.props.skill;

		return (
			<div className="individual-tech">
				<img className="tech-logo" src={process.env.PUBLIC_URL + `/${name}.png`} alt={`${name}`} />
				<p>{name}</p>
			</div>
		)
	}
}

TechSkillItem.propTypes = {
	skill: PropTypes.object.isRequired
}


export default TechSkillItem