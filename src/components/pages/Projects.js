import React from 'react';
import PropTypes from 'prop-types';
import '../../css/Projects.css';
import ProjectItem from '../ProjectItem';

export class Projects extends React.Component {

	render() {

		const projects = this.props.projects.map(function(project) {
			return <ProjectItem key={project.id} project={project} />
		}) 
		
		return (
			<div className="projects-grid-container">
				{projects}
			</div>
		)
	}
}

Projects.propTypes = {
	projects: PropTypes.array.isRequired
}

export default Projects