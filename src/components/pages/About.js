import React from 'react';
import '../../css/About.css';
import {Link} from 'react-router-dom';
// import AboutButton from '../AboutButton';
// import * as uuid from 'uuid';

export class About extends React.Component {

	state = {
		aboutButtons: [

		// should use uuid here for id but for sake on function styling example hardcoded instead
			{
				id: 0,
				link: "https://drive.google.com/open?id=1Esh2sFFMaB3K3FnVfu9XqsmQLNqx3zK9",
				text: "CV",
			},

			{
				id: 1,
				link: "https://gitlab.com/Oduu",
				text: "GitLab",
			},

			{
				id: 2,
				link: "/projects",
				text: "Projects",
			},

		]
	}


	render() {
/*
		const aboutButtons = this.state.aboutButtons.map(function(button) {
			return <AboutButton key={button.id} button={button} />
		}) */

		return (
			<div className="About">
				<div className="about-column">
					<h1 className="candidate-name">Henry Redding</h1>
					<div className="personal-summary">
						<h4>A bit about me:</h4>
						
						<p>I'm currently looking for my first job in the software development industry, ideally in the Birmingham area.</p>
						
						<p>I used to work at a finance company in a website support/admin role but left to pursue my passion.</p>
						{/*<p>I've spent the last few months developing my skills and creating projects of my own.</p>*/}
						
						<p>Please have a look through this portfolio site or download my CV to learn more about me.</p>
						
						<a id="portfolio-git" href="https://gitlab.com/Oduu/interactive-portfolio" target="_blank" rel="noopener noreferrer">

							<p>If you would like to checkout the code base for this website click here.</p>

						</a>

						<p>If you think I would be an ideal candidate for a job you have available, 
						please contact me via one of the methods below.</p>
						
					</div>
				</div>
{/*				<div className="further-details-column">
					<h1 className="column-title">Take a look:</h1>
					<div className="about-navigation">

						{aboutButtons}

					</div>
				</div>*/}

				<div className="contact-information">

					<div className="contact-box email">
						<i className="fa fa-envelope fa-5x"></i>
						<div className="info email-info">
							<p>Email</p>
							<p>reddinghf@gmail.com</p>
							
						</div>
						
					</div>

					<div className="contact-box phone">
						<i className="fa fa-phone fa-5x"></i>
						<div className="info">
							<p>Phone</p>
							<p>07749798992</p>
						</div>
						
					</div>

					<div className="contact-box cv">
						<a href={process.env.PUBLIC_URL + `/CV.pdf`} target="_blank" rel="noopener noreferrer">
						<i className="cv-link fa fa-download fa-5x"></i>
						</a>
						<div className="info">
							<p>CV</p>
							<a href={process.env.PUBLIC_URL + `/CV.pdf`} download >PDF download</a>
							{/*<p onClick={() => {window.location.href='https://drive.google.com/file/d/1ReZkDo4qcym6zo1UQH6pMVAdmoFc8uwG'}} id="g-doc">Google Docs</p>*/}
						</div>
						
					</div>

				</div>
				<Link to="/projects">
				
				<div className="see-projects">
					<p>Check out my work</p>
					<i className="fa fa-arrow-right"></i>
				</div>
				</Link>
			</div>
		)
	}
}

export default About;