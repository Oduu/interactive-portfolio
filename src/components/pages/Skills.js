import React from 'react';
import '../../css/Skills.css';
import * as uuid from 'uuid';
import TechSkillItem from '../TechSkillItem';
import PersonalSkillItem from '../PersonalSkillItem'

export class Skills extends React.Component {

	state = {
		tech_skills: [

			{
				id: uuid.v4(),
				name: "PHP",
				logo: ""
			},

						{
				id: uuid.v4(),
				name: "React",
				logo: ""
			},


			{
				id: uuid.v4(),
				name: "Python",
				logo: "https://www.stickpng.com/assets/images/5848152fcef1014c0b5e4967.png"
			},

			{
				id: uuid.v4(),
				name: "JavaScript",
				logo: "https://upload.wikimedia.org/wikipedia/commons/6/6a/JavaScript-logo.png"
			},

			{
				id: uuid.v4(),
				name: "SQL",
				logo: "https://www.stickpng.com/assets/images/5848104fcef1014c0b5e4950.png"
			},





			{
				id: uuid.v4(),
				name: "HTML",
				logo: "https://cdn.pixabay.com/photo/2017/08/05/11/16/logo-2582748_960_720.png"
			},

			{
				id: uuid.v4(),
				name: "CSS",
				logo: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/CSS.3.svg/1200px-CSS.3.svg.png"
			},

			{
				id: uuid.v4(),
				name: "Git",
				logo: "https://git-scm.com/images/logos/downloads/Git-Icon-1788C.png"
			},

			{
				id: uuid.v4(),
				name: "Vue.js",
				logo: "https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Vue.js_Logo_2.svg/1200px-Vue.js_Logo_2.svg.png"
			},


		],

		personal_skills: [

			{
				id: uuid.v4(),
				name: "Problem Solving",
				description: `My previous role relied on this a lot to be able to diagnose technical issues 
							and find the cause which then needed to be communicated to the development team.`
			},

			{
				id: uuid.v4(),
				name: "Co-ordination",
				description: `I excelled at this by enforcing a ticket system with employees.
							This allowed me to perform underpressure by being able to prioritise issues from across the company
							and inform these details to external companies. I was then required to relay updates to
							other 3rd parties and the rest of the business to keep work moving consistently and to organise system upgrades.`
			},

			{
				id: uuid.v4(),
				name: "Communication",
				description: `Linking in with my co-ordination experience, I also was effective at communicating with external and internal teams
							about current issues and how to resolve them which helped processes move faster even when spread across multiple
							companies across the country. I also volunteered to run training sessions as I felt comfortable being able to explain
							to employees on how best to complete there required roles.`
			},

		]
	}

	render() {

		const tech_skills = this.state.tech_skills.map(function(skill) {
			return <TechSkillItem key={skill.id} skill={skill} />
		})

		const personal_skills = this.state.personal_skills.map(function(skill) {
			return <PersonalSkillItem key={skill.id} skill={skill} />
		})

		return (
			<div className="skills">
				<div className="tech-column">
					<h1 className="column-title">Technical Skills</h1>
					<div className="tech-list">

					{tech_skills}

					</div>
				</div>

				<div className="skills-column">
					<h1 className="column-title">Key Personal Skills</h1>
					<div className="personal-skills">

					{personal_skills}

					</div>
				</div>
			</div>
		)
	}
}

export default Skills