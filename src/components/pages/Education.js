import React from 'react';
import '../../css/Education.css';

class Education extends React.Component {

  state = {

      GCSE: [

        { subject: "Maths", grade: "A*" },
        { subject: "Business", grade: "A" },
        { subject: "History", grade: "A" },
        { subject: "ICT", grade: "A" },
        { subject:"Computing", grade: "A" },
        { subject:"Electronics", grade: "A" },
        { subject:"Biology", grade: "A" },
        { subject:"Chemistry", grade: "A" },
        { subject:"Physics", grade: "A" },
        { subject:"English Literature", grade: "A" },
        { subject:"English Language", grade: "A" },

      ],

      AS: [

        { subject: "Computing", grade: "A" },
        { subject: "Economics", grade: "A" },
        { subject: "Maths", grade: "A" },
        { subject: "Engineering", grade: "A" },

      ],

      A2: [

        { subject: "Computing", grade: "B" },
        { subject: "Economics", grade: "B" },
        { subject: "Maths", grade: "B" },

      ]

    }

  render() {

    const gcse = Object.keys(this.state.GCSE).map((key, value) => {
      return <tr className="gcse-row" key={key}><td className="subject">{this.state.GCSE[key].subject}</td><td>{this.state.GCSE[key].grade}</td></tr>
    })

    const as_levels = Object.keys(this.state.AS).map((key, value) => {
      return <tr className="as-row" key={key}><td className="subject">{this.state.AS[key].subject}</td><td>{this.state.AS[key].grade}</td></tr>
    })

    const a2_levels = Object.keys(this.state.A2).map((key, value) => {
      return <tr className="a2-row" key={key}><td className="subject">{this.state.A2[key].subject}</td><td>{this.state.A2[key].grade}</td></tr>
    })



    return (

      <div className="Education">

      <div className="intro">
        <p>I attended Haybridge High School and Sixth form for my secondary school and college years</p>
      </div>

        <div className="education-grid">

          <div className="a2-container">
            <h2>A2</h2>
            <table>
              <tbody> 
                {a2_levels}
              </tbody>
            </table>
          </div>

          <div className="as-container">
            <h2>AS Levels</h2>
            <table>
              <tbody> 
                {as_levels}
              </tbody>
            </table>

          </div>


          <div className="gcse-container">
            <h2>GCSE</h2>
            <table>
              <tbody> 
                {gcse}
              </tbody>
            </table>
          </div>




        </div>
      </div>

    )
  }

}

export default Education;
