import React from 'react';
import PropTypes from 'prop-types';

export class AboutButton extends React.Component {

	getStyle = (buttonIndex) => {
		const buttonColors = [ '#61d0d4', '#6561d4', '#d46361']
		return {
			background: buttonColors[buttonIndex]
		}
	}

	render() {

		const { id, link, text } = this.props.button

		return (
			<div>
				<a 
					href={link}
					target="_blank"
					style={this.getStyle(id)}
					rel="noopener noreferrer"
					className="button round-button about-button">
					{text}
				</a>
			</div>
		)
	}
}

AboutButton.propTypes = {
	button: PropTypes.object.isRequired,
}

export default AboutButton