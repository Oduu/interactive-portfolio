import React from 'react';
import PropTypes from 'prop-types';
import '../css/ProjectItem.css';

export class ProjectItem extends React.Component {

	render() {

		const { image, title, tech, description , git } = this.props.project



		return (
			<div className="project-item">

				<div className="project-title">
					
					<div className="project-links" onClick={() => {window.open(git)}}>
						{/*<a href={git} >Git</a>*/}

						<img src={process.env.PUBLIC_URL + `/gitlab.png`}
						alt="gitlab"

						onMouseOver={e => e.currentTarget.src = process.env.PUBLIC_URL + `/gitlab-black.png`}
						onMouseLeave={e => e.currentTarget.src = process.env.PUBLIC_URL + `/gitlab.png`}
						target="_blank"
						/>

						{/* <a href="https://gitlab.com/Oduu">Live version</a> */}
						<h4  className="project-title">{title}</h4>
					</div>

					
				</div>

				<img className="project-screenshot" src={image} alt="project screenshot here"/>
				<div className="project-details">
					
					<p className="project-description">{description}</p>
					<p className="project-tech">{tech}</p>
					
				</div>

			</div>
		)
	}
}

ProjectItem.propTypes = {
	project: PropTypes.object.isRequired
}

export default ProjectItem