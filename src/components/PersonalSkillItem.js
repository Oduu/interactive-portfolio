import React from 'react';
import PropTypes from 'prop-types';

export class PersonalSkillItem extends React.Component {
	render() {

		const { name, description } = this.props.skill

		return (
			<div className="individual-skill">
				<h4 className="skill-title">{name}</h4>
				<ul className="skill-expansion">
					<li>
					{description}
					</li>
				</ul>
			</div>
		)
	}
}

PersonalSkillItem.propTypes = {
	skill: PropTypes.object.isRequired
}

export default PersonalSkillItem