# Online resume/interative portfolio project

## Static site using React

A simple React CV style project with responsive design

Desktop:
![](https://i.imgur.com/s6jdeWo.png)

Mobile:
![](https://i.imgur.com/3v39gYB.png)
